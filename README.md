# Xano Ably Basic Demo

The demo illustrates the basic usage of Ably with Xano as your backend.

## Core Structure
The folder structure is intended to be clean as possible. Specs have been removed.

### Setup & Dependencies

#### List of Dependencies
* material
* bootstrap css
* lodash-es
* Ably
#### Setup

You can use either npm or yarn to install dependencies.

run `npm install` or `yarn`

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

