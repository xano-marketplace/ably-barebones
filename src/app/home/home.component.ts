import {Component, OnInit} from '@angular/core';
import {ConfigService, XanoConfig} from '../config.service';
import {AblyService} from '../ably.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {BehaviorSubject} from 'rxjs';
import {MatSnackBar} from '@angular/material/snack-bar';
import {get} from 'lodash-es';
import {debounceTime} from 'rxjs/operators';
import {SnackbarResponseComponent} from '../snackbar-response/snackbar-response.component';

@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

	public ablyForm: FormGroup = new FormGroup({
		channel: new FormControl('', [Validators.required, Validators.pattern("^[a-zA-Z0-9_\-]*$")]),
		event_name: new FormControl(''),
		data: new FormControl(null),
	});
	public data = new BehaviorSubject([{key: '', value: ''}]);
	public config: XanoConfig;
	public configured: boolean = false;

	constructor(
		private configService: ConfigService,
		private ablyService: AblyService,
		private snackbar: MatSnackBar
	) {
	}

	ngOnInit(): void {
		this.config = this.configService.config;
		this.configService.xanoApiUrl.subscribe(apiUrl => this.configured = !!apiUrl);

		this.ablyForm.controls.channel.valueChanges.pipe(debounceTime(500)).subscribe(res => {
			if (res?.length) {
				const channel = this.configService.ably.channels.get(res);
				channel.subscribe((message) => {
					this.snackbar.openFromComponent(SnackbarResponseComponent, {
						data: message,
						duration: 10000
					});
				});
			}
		});
	}

	addData() {
		this.data.next([...this.data.value, {key: '', value: ''}]);
	}

	removeData(rowID) {
		let data = [...this.data.value];
		data.splice(rowID, 1);
		this.data.next(data);
	}

	public submit(): void {
		this.ablyForm.markAllAsTouched();

		if (this.ablyForm.valid) {
			let data = [];
			for (let item of this.data.value) {
				if (item.key !== '' && item.value !== '') {
					let keyValue  = {};
					keyValue[item.key] = item.value;
					data.push(keyValue);
				}
			}
			if (data.length) {
				this.ablyForm.controls.data.patchValue(data);
			}

			this.ablyService.ably(this.ablyForm.getRawValue()).subscribe(res => {
				this.ablyForm.reset();
				this.data.next([{key: '', value: ''}]);
			}, error => {
				this.snackbar.open(get(error, 'error.message', 'An Error Occurred'), 'Error', {
					panelClass: 'error-snack',
				});
			});
		}
	}

}
