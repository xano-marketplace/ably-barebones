import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {XanoService} from './xano.service';
import {ApiService} from './api.service';
import * as Ably from 'ably';

export interface XanoConfig {
	title: string,
	summary: string,
	editText: string,
	editLink: string,
	descriptionHtml: string,
	logoHtml: string,
	requiredApiPaths: string[]
}

@Injectable({
	providedIn: 'root'
})


export class ConfigService {
	public xanoApiUrl: BehaviorSubject<any> = new BehaviorSubject<any>(null);
	public ablyKey: BehaviorSubject<any> = new BehaviorSubject<any>(null);
	public ably;

	public config: XanoConfig = {
		title: 'Ably Core Demo',
		summary: 'Use real-time functionality from Ably with Xano as your backend.\n',
		editText: 'Get source code',
		editLink: '',
		descriptionHtml: `
                <h2>Description</h2>
                <p>
					This demo showcases the flexibility of using Ably with Xano as your backend. Ably enables you to provide complete 
					real-time functionality to your users, like notifications, chat, and other real-time data flows.
				</p>  
				<p>
					The demo consists of a channel input, an event name input, and a dynamic table for metadata that you can send to Ably. 
					You can do this on your front-end, as shown in the demo. Or you could call the Ably endpoint from another API endpoint 
					in Xano when you have an update that you'd like to publish to Ably channel subscribers.
				</p> 
				<h2>Getting Started</h2>
				<ul>
					<li>Install the extension in your Xano Workspace</li>
					<li>Create an <a href="https://www.ably.io/" target="_blank">Ably</a> account or sign in to an existing one.</li>
					<li>Create an app by clicking <b>+ Create New App</b> in your Ably dashboard or click on an existing one.</li>
					<li>
						Once in your Ably app, go to the API Keys tab, copy an existing API key, or click <b>+ Create a new API key</b> 
						in the upper right corner. Make sure to enable capabilities to publish and subscribe. 
						(Note: these settings can be configured in the API Keys tab of the control panel in your Ably app).
					</li>
					<li>Copy this key and set it as your environment variable <b>ably_api_key</b> in your Xano workspace by clicking configure on the extension page.   </li>
					<li>Next, go to your Xano API group, called "ably" and copy the Base Request URL.</li>
					<li>Finally in the demo click <b>Test with your Xano Account…</b>, input your Base Request Url and your Ably API Key, and click  <b>Update Settings</b>.</li>
					<li>You’re ready to test drive!</li>
				</ul>
				<h2>Components</h2>
				<ul>
					<li>
						<b>Home</b>
						<p>
							Here you can fill out the payload you want to publish to Ably; it is a table that consists of a channel input 
							and event name input, where you can dynamically add your data that you wish to send.
						</p>
					</li>
					<li>
						<b>Snackbar</b>
						<p>
							The demo dynamically subscribes to whatever channel you input in the form so that you can see the real-time 
							update from Ably as a notification popup.
						</p>
					</li>
					<li>
						<b>Config Panel</b>
						<p>This is where you set your Xano API URL and Ably Key.</p>
					</li>
				</ul>
		`,
		logoHtml: '',
		requiredApiPaths: [
			'/ably'
		]
	};

	constructor(private apiService: ApiService, private xanoService: XanoService) {
		this.ablyKey.asObservable().subscribe(res => {
			if (res) {
				this.ably = new Ably.Realtime(res);
			}
		});
	}

	public isConfigured(): Observable<any> {
		return this.xanoApiUrl.asObservable();
	}

	public configGet(apiUrl): Observable<any> {
		return this.apiService.get({
			endpoint: this.xanoService.getApiSpecUrl(apiUrl),
			headers: {
				Accept: 'text/yaml'
			},
			responseType: 'text',
		});
	}

}
