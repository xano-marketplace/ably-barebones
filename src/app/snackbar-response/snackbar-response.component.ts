import {Component, Inject, OnInit} from '@angular/core';
import {MAT_SNACK_BAR_DATA, MatSnackBar} from '@angular/material/snack-bar';

@Component({
	selector: 'app-snackbar-response',
	templateUrl: './snackbar-response.component.html',
	styleUrls: ['./snackbar-response.component.scss']
})
export class SnackbarResponseComponent implements OnInit {

	constructor(
		@Inject(MAT_SNACK_BAR_DATA) public data,
		public snackbar: MatSnackBar
	) {
	}

	ngOnInit(): void {
	}

}
