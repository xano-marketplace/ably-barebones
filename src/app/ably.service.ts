import {Injectable} from '@angular/core';
import {ApiService} from './api.service';
import {ConfigService} from './config.service';
import {Observable} from 'rxjs';

@Injectable({
	providedIn: 'root'
})
export class AblyService {

	constructor(
		private apiService: ApiService,
		private configService: ConfigService
	) {
	}

	public ably(ablyPayload): Observable<any> {
		return this.apiService.post({
			endpoint: `${this.configService.xanoApiUrl.value}/ably`,
			params: ablyPayload,
		});
	}

}
